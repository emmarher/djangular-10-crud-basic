# Instalacion

Crear entorno virtual de python3
> python3 -m venv venv

(entorno virtual con el nombre de _venv_)

> $ pip install -r requirements.txt


## Ejecutar el servidor

Es necesario ubicarse en la ruta dentro de la carpeta _backend_, dode se encuentra el archivo __manage.py__.

**Es necesario tener activado el entorno virtual de python3 previamente creado**
> source venv/bin/activate

Comando para iniciar el servidor:

```
python manage.py runserver
```

## Configurar context_processors
Necesitamos permitir peticiones a nuestra aplicacion de django desde otros origines.
En este ejemplo, vamos a configurar CORS para aceptar solicitudes desde _localhost:4200_, este es la ruta priveniente del frontend

Primero, instala la libreria _django-cors-headers_
> pip3 install django-cors-headers

En "settings.py", agregar la configuracion para cors

- CORS_ORIGIN_ALLOW_ALL : Si True, se aceptarán todos los orígenes (no utilice la lista blanca a continuación). Predeterminado a False.
- CORS_ORIGIN_WHITELIST : lista de orígenes que están autorizados para realizar solicitudes HTTP entre sitios. Predeterminado a [].    


## Crear clase de serializador para el modelo de datos

Vamos a crear TutorialSerializerclase que gestionará la serialización y deserialización desde JSON.

Hereda de rest_framework.serializers.ModelSerializersuperclase que llena automáticamente un conjunto de fieldsy por defecto validators. Necesitamos especificar la clase del modelo aquí.

tutoriales / serializadores.py
